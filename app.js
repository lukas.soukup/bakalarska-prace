const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');

app.use(express.static('ClientSide'));
app.get('/', function(req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, '/ClientSide') });
});
app.get('/app', function(req, res) {
    res.sendFile('app.html', { root: path.join(__dirname, '/ClientSide') });
});
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//course error handeling 
//umoznuje odesilat req z SPA, atd...
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); //umoznuje pristup pro vsechny clienty
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, PATCH, DELETE");
        return res.status(200).json({});
    }
    next(); //pro preposlani do dalsiho routeru
});

//middleware ktery odchytava requesty mirene na neexistujci odkaz
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

//middleware ktery odchytava vsechny ostatni errory
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;