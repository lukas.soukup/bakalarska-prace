# Bakalářská práce 
### Akademický rok: 2020/2021
	Vedoucí práce: doc. Ing. Josef Chaloupka, Ph.D.
	Ústav informačních technologií a elektroniky
## Zadání: 
1. Vytvořte univerzální webové rozhraní, které umožní práci se sdruženými daty obrázků a příslušného textu.  
2. První část webového rozhraní bude sloužit pro import a prohlížení jednotlivých dat. Zde vytvořte administraci webových stránek (přihlašování) a administraci vkládání obrazů a příslušných textů.  
3. V druhé části realizujte vyhledávání klíčových slov (kratších větných spojení), kde prohledávání bude realizováno v přepsaných textech jednotlivých obrázků (skenů), přičemž přepsané texty budou odkazově spojeny s jednotlivými skeny.  
4. Navržené webové rozhraní otestujte na archivu ročenek německého Jizersko-ješťedsko-horského spolku a jejich OCR přepisů.  

## Funkční Verze programu 
**[https://ocr-drive-editor.herokuapp.com/index.html](https://ocr-drive-editor.herokuapp.com/index.html)**  
Aplikace je v režimu *testing* a mohou ji používat pouze přidaní uživatelé. Všechny změny uložené zde se projeví v aplikaci.  
  
Byl vytvořen zkušební Google účet k testování archivu ročenek německého Jizersko-ješťedsko-horského spolku a jejich OCR přepisů:  
**Email:** nodejs.api01@gmail.com  
**Heslo:** Nodejs.API01  

## Způsob vypracování:
Aplikace využívá API od Google - **Google Drive API** v2 pouze pro klientskou část aplikace.  
	
![GDrive_overwiev](https://developers.google.com/drive/images/drive-intro.png)

Přihlašování do aplikace probíhá skrz OAuth2 algoritmus od Googlu. V závěru vývoje aplikace bude mít úvodní stránku s přehlednými vysvětlivkami jak správně aplikaci používat a také bude obsahovat přihlášovací tlačítko. Po úspěšném přhlášení se uživatel dostane k samotné aplikaci.  

## Předpoklady ke správnému používání aplikace:
Na Google Drivu musíte mít obrázky ve formátu *.bmp .jpg .png .pbm* a textový dokument ve formátu Google Doc.  
Google umožňuje automaticky převést textové soubory do Google Doc, postup na povolrní tohoto nastavení je **[zde](https://alicekeeler.com/2014/09/20/new-google-drive-always-convert-office-documents/)**   
Dále v menu aplikace se zobrazují pouze složky a obrázky, po zvolení obrázku se zobrazí zároveň s ním i **STEJNOJMENNÝ** textový dokument. Pokud tento dokument není nalezen, aplikace umožní obrázek převést na text pomocí OCR technologie s knihovnou **Tesseract.js**  
   
Mimo jinné aplikace také umožnuje procházet **sdílené soubory**, přepínat mezi sdílenýmy a osobními soubory bude ve složce nastavení.  

*	**Interaktivní obsah, nově implementováno. Aplikace je nyní schopna rozpoznat čísla stránek a podle nich najít příslušné soubory a po zvolení je i zobrazit.**  
### Postup vytvoření obsahu pro uživatele:
1. Nalezněte obrázek, ze kterého chcete vytvořit interaktivní obsah. Mělo by se jednat o obrázek jehož dokumennt obsahuje odstavce začínající řetězcem: [**#[číslo]** ] *(výchozí)* - 1.# 2. číslo stránky 3.mezera. Tento řetezec lze jednoduše změnit v menu interaktivního obsahu  
2. Klikněte pravým tlačítkem myši na zvolený obrázek a zvolte *Nastavit jako stránku s obsahem*  
3. Zobrazí se nové menu s odkazy na jednotlivé odstavce. Pokud se nic nezobrazí, nejspíše odstavce Google Dokumentu příslušného obrázku nezačínají správným řetězcem.  
	> Algoritmus je funkční, ale není 100% korektní, občas se stane, že spojí číslo stránky s naprosto odlišným souborem a nebo nenajde soubor vůbec. To je zapříčiněno chabým OCR překladem dokumentu. Ačkoliv algoritmus obsahuje jakési primitivní korekce překladu, může se stát, že překlad bude natolik nečitelný, že číslo stránky jednoduše nelze nalézt.  
# Práce byla dokončena dne 19. 7. 2021
	Již se nebudou provádět žádné úpravy, pokud to nebu nutné!
