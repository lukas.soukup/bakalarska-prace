// Client ID and API key from the Developer Console
var CLIENT_ID = '927040980997-iioq8r2b7iqb1tapbkdlqr7ae133oaet.apps.googleusercontent.com';
var API_KEY = 'AIzaSyBOOExHo3M6BWGFu6aoUGREzAUoDHvyTxk';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = 'https://www.googleapis.com/auth/drive';

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
var GoogleAuth;
function initClient() {
  gapi.client.init({
    apiKey: API_KEY,
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    GoogleAuth = gapi.auth2.getAuthInstance();
    console.log(GoogleAuth.currentUser.get());
    GoogleAuth.isSignedIn.listen(updateSigninStatus);
    // Handle the initial sign-in state.
    updateSigninStatus(GoogleAuth.isSignedIn.get());
  }, function(error) {
    console.log(JSON.stringify(error, null, 2));
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    if (GoogleAuth.isSignedIn.get()) {
      hideWelcomeLayer();
      var user = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
      console.log(user)
      document.getElementById('userName').innerHTML = user.getName();
      document.getElementById('userImg').src = user.getImageUrl();
      document.getElementById('userEmail').innerHTML = user.getEmail();
    }
    listFolders('', document.getElementById("sharedFileSwitch").checked);
  } else {
    if(!isLoggingOut) handleAuthClick();
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn({ux_mode: 'redirect'});
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick() {
  gapi.auth2.getAuthInstance().signOut();
}