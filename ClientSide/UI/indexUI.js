//----------------Header-----------------------------------------------
var header = document.getElementById("heading");
var appName = document.getElementById("appName");
var appNameText = document.getElementById("name");
var authBtn =  document.getElementById("signIn");
var appIcon = document.getElementById("appIcon");

window.onscroll = function() {onScrollEvent()};

function onScrollEvent() {
    if (document.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        header.style.width = "100%";
        header.style.height = "50px";
        header.style.backgroundColor = "#f1f1f1f1";
        header.style.position = "fixed";
        header.style.marginTop = "-27.5px";
        header.style.overflow = "hidden";
        header.style.textAlign = "";
        header.style.padding = "30px 0px 5px 0px";

        appName.style.display = "inline";
        appName.style.margin = "";
        appName.style.marginLeft = "100px";
        appName.style.backgroundColor = "#ffffff";
        appName.style.height = "100%";
        appName.style.borderRadius = "20px";
        appName.style.padding = "10px 20px 10px 0px";
        appName.style.boxShadow = "none";

        appIcon.style.width = "45px";
        appIcon.style.height = "45px";
        appIcon.style.position = "absolute";        
        appIcon.style.margin = "3px 0px 0px 10px";

        appNameText.style.width = "auto";
        appNameText.style.display = "inline-block";
        appNameText.style.padding = "15px 0px 0px 70px";
        appNameText.style.textDecoration = "none";
        appNameText.style.color = "black";
        appNameText.style.textShadow = "0 1px black";
        appNameText.style.fontWeight = "bold";
        appNameText.style.textAlign = "center";
        appNameText.style.marginTop = "0px";

        authBtn.style.float = "right";
        authBtn.style.margin = "6px 6px 0px 0px";

        document.getElementById("infotext").style.paddingTop = "150px";

      } else {
          header.style.width = "100%";
        header.style.height = "auto";
        header.style.backgroundColor = "transparent";
        header.style.position = "relative";
        header.style.overflow = "auto";
        header.style.textAlign = "center";
        header.style.padding = "0px 0px 50px 0px";
        header.style.marginTop = "";

        appName.style.display = "inline-block";
        appName.style.margin = "20px 20px";
        appName.style.backgroundColor = "transparent";
        appName.style.height = "";
        appName.style.borderRadius = "10px 10px 100px 100px";
        appName.style.padding = "";
        appName.style.boxShadow = "0px 0px 5px 10px";

        appIcon.style.width = "";
        appIcon.style.height = "";
        appIcon.style.position = "";        
        appIcon.style.margin = "";

        appNameText.style.width = "250px";
        appNameText.style.display = "";
        appNameText.style.padding = "5px 5px 0px 5px";
        appNameText.style.textDecoration = "";
        appNameText.style.color = "";
        appNameText.style.textShadow = "0 1px black";
        appNameText.style.fontWeight = "bold";
        appNameText.style.textAlign = "center";
        appNameText.style.marginTop = "";

        authBtn.style.float = "";
        authBtn.style.margin = "";
        
        document.getElementById("infotext").style.paddingTop = "";
    }
}